<?php
//Include the code
require_once 'phplot/phplot.php';
//Define the object
$plot = new PHPlot(800,600);
//Set titles
$plot->SetPlotType('lines');
$plot->SetDataType('text-data');
$data = array();
for ($x = 0; $x <= 5; $x++) $data[] = array('', $x+1, $x*$x/2, $x*$x*$x/3);


$plot->SetDataValues($data);
$plot->SetXDataLabelPos('none');
$plot->SetLineWidths(3);

$plot->SetDrawXGrid(True);


$plot->DrawGraph();



?>
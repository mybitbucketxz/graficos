<?php
//Include the code
require_once 'phplot/phplot.php';
//Define the object
$plot = new PHPlot(800,600);
//Set titles
$plot->SetPlotType('bars');
$plot->SetDataType('text-data');
$data = array(
 array('a',3,4,2),
 array('b',5,'',1), // here we have a missing data point, that's ok
 array('c',7,2,6),
 array('d',8,1,4),
 array('e',2,4,6),
 array('f',6,4,5),
 array('g',7,2,3)
);
$plot->SetDataValues($data);
$plot->SetXDataLabelPos('none');
$plot->SetLineWidths(3);

$plot->SetDrawXGrid(True);


$plot->DrawGraph();



?>
<?php
//Include the code
require_once 'phplot/phplot.php';
//Define the object
$plot = new PHPlot(800,600);
//Set titles
$plot->SetPlotType('lines');
$plot->SetDataType('text-data');
$data = array(
 array('', 0, 0, 0, 0),
 array('', 1, 1, 1, -10),
 array('', 2, 8, 4, -20),
 array('', 3, 27, 9, -30),
 array('', 4, 64, 16, -40),
 array('', 5, 125, 25, -50),
);

$plot->SetDataValues($data);
$plot->SetXDataLabelPos('none');
$plot->SetLineWidths(3);

$plot->SetDrawXGrid(True);


$plot->DrawGraph();



?>